import 'package:geopoint/geopoint.dart';

class City {
  String id;
  String name;
  String description;
  GeoPoint geopoint;
  List<String> gallery;

  City({ this.id, this.name, this.description, this.geopoint, this.gallery});
}

List<City> listCity = [
  City(
    id: "5",
    name: "Bamenda",
    description: "Ceci est la description de la ville de Bamenda. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Bamenda"),
    gallery: ["assets/City/Bamenda/1.jpg", "assets/City/Bamenda/2.jpg", "assets/City/Bamenda/3.jpg"],
  ),

  City(
    id: "4",
    name: "Buea",
    description: "Ceci est la description de la ville de Buéa. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Buéa"),
    gallery: ["assets/City/Buea/1.jpg", "assets/City/Buea/2.jpg", "assets/City/Buea/3.jpg"],
  ),

  City(
    id: "2",
    name: "Douala",
    description: "Ceci est la description de la ville de Douala. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Douala"),
    gallery: ["assets/City/Douala/1.jpg", "assets/City/Douala/2.jpg", "assets/City/Douala/3.jpg"],
  ),

  City(
    id: "1",
    name: "Grand Nord",
    description: "Ceci est la description de la ville de Grand Nord. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Grand Nord"),
    gallery: ["assets/City/Grand_Nord/1.jpg", "assets/City/Grand_Nord/2.jpg", "assets/City/Grand_Nord/3.jpg"],
  ),

  City(
    id: "3",
    name: "Ouest",
    description: "Ceci est la description de la ville de Ouest. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Ouest"),
    gallery: ["assets/City/Ouest/1.jpg", "assets/City/Ouest/2.jpg", "assets/City/Ouest/3.jpg"],
  ),

  City(
    id: "1",
    name: "Yaounde",
    description: "Ceci est la description de la ville de Yaoundé. C'est une description assez brève qui a pour but de remplir le cahmps de la description de cette ville en attendant les données réels",
    geopoint: GeoPoint(latitude: 4.58942, longitude: 9.264852, altitude: 10, name: "Yaoundé"),
    gallery: ["assets/City/Yaounde/1.jpg", "assets/City/Yaounde/2.jpg", "assets/City/Yaounde/3.jpg"],
  ),
];