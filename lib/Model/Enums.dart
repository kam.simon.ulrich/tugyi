enum Activity {
  defaults,
  dance,
  painting,
  singing,
  cooking,
  writing,
}

enum Category {
  Dance,
  Cinema,
  Gastronomy,
  Cuisine,
  Art_Plastique,
  Musique,
  Mode,
  Centre_culturel,
  Nature,
}