import 'dart:async';

import 'package:flutter/material.dart';
import 'package:trevatell_template/DataSample/HotelListData.dart';
import 'package:trevatell_template/Library/SupportingLibrary/Ratting/Rating.dart';
import 'package:shimmer/shimmer.dart';
import 'package:trevatell_template/Model/Place.dart';
import 'package:trevatell_template/UI/B1_Home/Hotel/Hotel_Detail_Concept_1/hotelDetail_concept_1.dart';
import 'package:trevatell_template/UI/B1_Home/Hotel/Hotel_Detail_Concept_2/maps.dart';

class placeList extends StatefulWidget {
  List<Place> places;
  bool recommended;
  placeList({@required this.places, this.recommended = false});

  @override
  _placeListState createState() => _placeListState();
}

class _placeListState extends State<placeList> {

  @override
  static hotelListData hotelData;
  bool loadImage = true;
  bool colorIconCard = false;
  bool chosseCard = false;
  bool colorIconCard2 = true;
  var loadImageAnimation;
  var imageLoaded;

  @override
  void initState() {

    Timer(Duration(seconds: 2), () {
      setState(() {
        loadImage = false;
      });
    });
    // TODO: implement initState
    super.initState();
  }

  Widget build(BuildContext context) {

    var loadImageAnimation = Container(
        color: Colors.white,
        child: ListView.builder(
          itemBuilder: (ctx, index) => cardLoading(hotelDataDummy[index]),
          itemCount: hotelDataDummy.length,
        ));

    var imageLoaded = Container(
        color: Colors.white,
        child: ListView.builder(
          itemBuilder: (ctx, index) => cardList(hotelData: hotelDataDummy[index], place: widget.places[index], recommended: widget.recommended,),
          itemCount: widget.recommended ? listPlaces2.length : hotelDataDummy.length,
        ));

    return Scaffold(
      body: Stack(
        children: <Widget>[
          chosseCard
              ? Padding(
              padding: const EdgeInsets.only(top: 95.0),
              child: Container(
                  color: Colors.white,
                  child: loadImage ? loadImageAnimation : mapView()))
              : Padding(
            padding: const EdgeInsets.only(top: 95.0),
            child: Container(
                color: Colors.white,
                child: loadImage ? loadImageAnimation : imageLoaded),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Container(
              color: Colors.white,
              height: 130.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                          padding: EdgeInsets.only(top: 35.0, left: 15.0),
                          child: Icon(
                            Icons.clear,
                            size: 30.0,
                          ))),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 13.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Lieux",
                          style: TextStyle(
                              fontFamily: "Sofia",
                              fontSize: 35.0,
                              fontWeight: FontWeight.w800),
                        ),
                        Row(
                          children: <Widget>[
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    if (colorIconCard == true) {
                                      colorIconCard = false;
                                      colorIconCard2 = true;
                                      chosseCard = false;
                                    } else {
                                      colorIconCard = true;
                                      colorIconCard2 = false;
                                      chosseCard = true;
                                    }
                                  });
                                },
                                child: Icon(
                                  Icons.calendar_view_day,
                                  color: colorIconCard
                                      ? Colors.black12
                                      : Colors.amber,
                                  size: 40,
                                )),
                            SizedBox(
                              width: 14.0,
                            ),
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    if (colorIconCard2 == true) {
                                      colorIconCard2 = false;
                                      colorIconCard = true;
                                      chosseCard = true;
                                    } else {
                                      colorIconCard2 = true;
                                      colorIconCard = false;
                                      chosseCard = false;
                                    }
                                  });
                                },
                                child: Icon(
                                  Icons.map_outlined,
                                  color: colorIconCard2
                                      ? Colors.black12
                                      : Colors.amber,
                                  size: 35,
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class mapView extends StatelessWidget {
  const mapView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Container(
      child: maps(),
    );
  }
}

class cardList extends StatelessWidget {
  @override
  var _txtStyleTitle = TextStyle(
    color: Colors.black87,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  var _txtStyleSub = TextStyle(
    color: Colors.black26,
    fontFamily: "Gotik",
    fontSize: 12.5,
    fontWeight: FontWeight.w600,
  );

  hotelListData hotelData;
  Place place;
  bool recommended;


  cardList({this.hotelData, this.place, this.recommended});
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => new PlaceDetail(
                place: place
            ),
            transitionDuration: Duration(milliseconds: 600),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            }));
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
        child: Container(
          height: 250.0,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12.withOpacity(0.1),
                    blurRadius: 3.0,
                    spreadRadius: 1.0)
              ]),
          child: Column(children: [
            Container(
              height: 165.0,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0)),
                image: DecorationImage(
                    image: AssetImage(place.gallery[0] == ""? hotelData.img : place.gallery[0]), fit: BoxFit.cover),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, right: 10.0),
                child: CircleAvatar(
                    radius: 20.0,
                    backgroundColor: Colors.black12,
                    child: Icon(
                      recommended ? Icons.favorite: Icons.favorite_border,
                      color: recommended? Colors.redAccent : Colors.white,
                    )),
              ),
              alignment: Alignment.topRight,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            width: 320.0,
                            child: Text(
                              place.name,
                              style: _txtStyleTitle,
                              overflow: TextOverflow.ellipsis,
                            )),
                        Padding(padding: EdgeInsets.only(top: 5.0)),
                        /*Row(
                          children: <Widget>[
                            ratingbar(
                              starRating: hotelData.rating,
                              color: Colors.deepPurpleAccent,
                            ),
                            Padding(padding: EdgeInsets.only(left: 5.0)),
                            Text(
                              "(" + hotelData.rating.toString() + ")",
                              style: _txtStyleSub,
                            )
                          ],
                        ),*/
                        Padding(
                          padding: const EdgeInsets.only(top: 4.9),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                size: 16.0,
                                color: Colors.black26,
                              ),
                              Padding(padding: EdgeInsets.only(top: 3.0)),
                              Text(place.city.name, style: _txtStyleSub)
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  /*Padding(
                    padding: const EdgeInsets.only(right: 13.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "\$" + hotelData.price,
                          style: TextStyle(
                              fontSize: 25.0,
                              color: Colors.deepPurpleAccent,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Gotik"),
                        ),
                        Text("per night",
                            style: _txtStyleSub.copyWith(fontSize: 11.0))
                      ],
                    ),
                  ),*/
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}

class cardLoading extends StatelessWidget {
  @override
  hotelListData data;
  cardLoading(this.data);
  final color = Colors.black38;
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
      child: Container(
        height: 250.0,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12.withOpacity(0.1),
                  blurRadius: 3.0,
                  spreadRadius: 1.0)
            ]),
        child: Shimmer.fromColors(
          baseColor: color,
          highlightColor: Colors.white,
          child: Column(children: [
            Container(
              height: 165.0,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0)),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, right: 10.0),
                child: CircleAvatar(
                    radius: 20.0,
                    backgroundColor: Colors.black12,
                    child: Icon(
                      Icons.favorite_border,
                      color: Colors.white,
                    )),
              ),
              alignment: Alignment.topRight,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 220.0,
                          height: 25.0,
                          color: Colors.black12,
                        ),
                        Padding(padding: EdgeInsets.only(top: 5.0)),
                        Container(
                          height: 15.0,
                          width: 100.0,
                          color: Colors.black12,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.9),
                          child: Container(
                            height: 12.0,
                            width: 140.0,
                            color: Colors.black12,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 13.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 35.0,
                          width: 55.0,
                          color: Colors.black12,
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.0)),
                        Container(
                          height: 10.0,
                          width: 55.0,
                          color: Colors.black12,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}
