import 'package:flutter/material.dart';
import 'package:google_directions_api/google_directions_api.dart';


class Directions extends StatefulWidget {
  const Directions({Key key}) : super(key: key);

  @override
  _DirectionsState createState() => _DirectionsState();
}

class _DirectionsState extends State<Directions> {

  @override
  void initState(){
    DirectionsService.init('AIzaSyA5RGi0HqYcwBA3xelEIM87p6oZ43zZStU');

    final directionsService = DirectionsService();

    final request = DirectionsRequest(
      origin: 'New York',
      destination: 'San Francisco',
      travelMode: TravelMode.driving,
    );

    directionsService.route(request,
            (DirectionsResult response, DirectionsStatus status) {
          if (status == DirectionsStatus.ok) {
            print(response);
            // do something with successful response
          } else {
            // do something with error response
            print(response);
          }
        });
  }
  @override
  Widget build(BuildContext context) {
    return Container(

    );
  }
}
